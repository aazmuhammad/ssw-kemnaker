# Naco (Kemnaker Account) PHP SDK

The Naco PHP SDK provides straight-forward and tested methods for accessing Authentication and Management API endpoints. This README describes how to get started and provides simple examples of how to use the SDK.

For more details about how to install this SDK into an existing project or how to download a preconfigured seed project, see:

* [Basic PHP example](https://codes.kemnaker.go.id/naker-php/naco-php-sdk/tree/master/examples)

### Dependencies

* PHP7.2+
* curl

## Installation

We recommend installing the SDK with [Composer](https://getcomposer.org/doc/00-intro.md). If you already have Composer installed globally, run the following:

```
$ composer require naco/sdk
```

Otherwise, [download Composer locally](https://getcomposer.org/doc/00-intro.md#locally) and run:

```
php composer.phar require naco/sdk
``` 

This will create `composer.json` and `composer.lock` files in the directory where the command was run, along with a vendor folder containing this SDK and its dependencies. 

Finally, include the Composer autoload file in your project to use the SDK:

```php
require '../vendor/autoload.php';

use Naco\Sdk\Client;
use Naco\Sdk\Config;

$config = new Config(
    'YOUR_CLIENT_ID',
    'YOUR_CLIENT_SECRET'
);
$auth = new Client($config);
```

## Usage - Authentication API

This SDK provides easy-to-implement methods to access the Authentication API. Some common authentication operations are explained below with examples.

The examples below assume that you followed the steps in the [Installation section](#installation) above.

### Login

The easiest and most secure way to handle logins to a web application is to use the [Authentication Code grant](https://tools.ietf.org/html/rfc6749#section-4.1) combined with Naco Login page. In short, that process is:

1. An user requesting access is redirected to the Naco (Kemnaker Account) Login Page.
2. The user is redirected back to your application's callback URL with a `code` and `state` parameter if successful or an `error` and `error_description` if not.
3. If the authentication was successful, the `state` parameter is validated.
4. If the `state` is valid, the `code` parameter is exchanged with Naco for an access token.
5. If the exchange is successful, the access token is used to call an Naco `/api/v1/users/me` endpoint, which returns the authenticated user's information.
6. This information can be used to create an account, to start an application-specific session, or to persist as the user session.

The PHP SDK handles most of the previous steps. Your application needs to:

1. Determine a log in action (for example: click a link, visit walled content, etc.) and call  `Naco\Sdk\Client::login`
2. Handle returned errors.

A simple implementation of these steps looks like this:

```php
require '../vendor/autoload.php';

use Naco\Sdk\Client;
use Naco\Sdk\Config;

$config = new Config(
    'YOUR_CLIENT_ID',
    'YOUR_CLIENT_SECRET'
);
$auth = new Client($config);

$scheme = isset($_SERVER['HTTPS']) ? "https" : "http";
$currentUri = $scheme . '://' . $_SERVER['HTTP_HOST'] . '/auth_code.php';

if (!isset($_GET['action'])) {
    if (null === $user = $auth->getUser()) {
        header('Location: ' . $auth->login('code', 'basic', $currentUri . '?action=auth'));
        exit;
    }

    header('Location: ' . $currentUri . '?action=welcome');
    exit;
}

if (isset($_GET['action']) && $_GET['action'] === 'auth') {
    if (null === $code = $auth->getAuthorizationCode()) {
        echo '<h1>Cannot Authenticate!!</h1>';
        exit;
    }

    $auth->issueToken(
        'authorization_code',
        'basic',
        [
            'code'         => $auth->getAuthorizationCode(),
            'redirect_uri' => $currentUri . '?action=auth',
            'state'        => $auth->getState(),
        ]
    );

    if (null === $auth->getAccessToken()) {
        echo '<h1>Failed Authenticate!!</h1>';
    }

    header('Location: ' . $currentUri . '?action=welcome');
    exit;
}

if (isset($_GET['action']) && $_GET['action'] === 'welcome') {
    if (null === $auth->getUser()) {
        header('Location: ' . $currentUri);
        exit;
    }

    // Access resource with your access tokem
    echo 'Token: <br />';
    var_dump($auth->getAccessToken());

    // Keep login with refresh token
    echo '<br /><br />Refresh Token: <br />';
    var_dump($auth->getRefreshToken());

    // Get authenticated user
    echo '<br /><br />User: <br />';
    var_dump($auth->getUser()->toArray());
}
```
