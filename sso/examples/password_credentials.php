<?php
/**
 * This file is part of the Naco Php Sdk package.
 *
 * (c) 2018 KEMNAKER RI <http://kemnaker.go.id>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '../vendor/autoload.php';

use Naco\Sdk\Client;
use Naco\Sdk\Config;

$config = new Config(
    'ee9faab6-e770-4144-8db0-f0ec4911e886',
    '1310bd86c6bc98b3e8add5e7a0c26d8f202b37479a98622a90'
);

$auth = new Client($config);

$auth->issueToken(
    'password',
    'basic',
    [
        'username' => 'iq.bluejack@gmail.com',
        'password' => 'rahasia',
    ]
);

// Do something with your token
echo 'Token: <br />';
var_dump($auth->getAccessToken());

echo '<br /><br />Token: <br />';
var_dump($auth->getUser()->toArray());
