<?php
/**
 * This file is part of the Naco Php Sdk package.
 *
 * (c) 2018 KEMNAKER RI <http://kemnaker.go.id>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '../vendor/autoload.php';

use Naco\Sdk\Client;
use Naco\Sdk\Config;

$config = new Config(
    'ee9faab6-e770-4144-8db0-f0ec4911e886'
);
$auth = new Client($config);

$scheme = isset($_SERVER['HTTPS']) ? "https" : "http";
$currentUri = $scheme . '://' . $_SERVER['HTTP_HOST'] . '/implicit.php';

if (!isset($_GET['action'])) {
    if (null === $user = $auth->getUser()) {
        header('Location: ' . $auth->login('token', 'basic email', $currentUri . '?action=auth'));
        exit;
    }

    header('Location: ' . $currentUri . '?action=welcome');
    exit;
}

if (isset($_GET['action']) && $_GET['action'] === 'auth') {
    // Do something with your token
    echo 'Token: <br />';
    var_dump($auth->getAccessToken());

    echo '<br /><br />Token: <br />';
    var_dump($auth->getUser()->toArray());
}
