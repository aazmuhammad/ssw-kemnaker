<?php
/**
 * This file is part of the Naco Php Sdk package.
 *
 * (c) 2018 KEMNAKER RI <http://kemnaker.go.id>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Naco\Sdk;

use DateTime;

/**
 * @author  Iqbal Maulana <iq.bluejack@gmail.com>
 */
final class User
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $name;

    /**
     * Constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }

        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getUpdatedAt(): ?DateTime
    {
        if (isset($this->data['updated_at'])) {
            return new DateTime($this->data['updated_at']);
        }

        return null;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->data;
    }
}
