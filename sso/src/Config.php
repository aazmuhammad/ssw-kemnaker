<?php
/**
 * This file is part of the Naco Php Sdk package.
 *
 * (c) 2018 KEMNAKER RI <http://kemnaker.go.id>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Naco\Sdk;

/**
 * @author  Iqbal Maulana <iq.bluejack@gmail.com>
 */
final class Config
{
    /**
     * @var string
     */
    private $uri;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * Constructor.
     *
     * @param string $clientId
     * @param string $clientSecret
     * @param string $uri
     */
    public function __construct(string $clientId, string $clientSecret = null, string $uri = 'https://account.kemnaker.go.id')
    {
        $this->uri = $uri;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }
}
