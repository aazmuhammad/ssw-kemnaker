<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use ReflectionClass;
use Throwable;
use LogicException;
use InvalidArgumentException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        LogicException::class,
        InvalidArgumentException::class,
        ModelNotFoundException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Throwable $exception
     *
     * @return void
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param Throwable                $exception
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        $response = parent::render($request, $exception);

        if ($exception instanceof ValidationException) {
            $response = $this->invalidJson($request, $exception);
        }

        return $response;
    }

    protected function prepareJsonResponse($request, Throwable $e)
    {
        return new JsonResponse(
            $this->convertExceptionToArray($e),
            $this->getErrorCode($e),
            $this->isHttpException($e) ? $e->getHeaders() : []
        );
    }

    protected function invalidJson($request, ValidationException $exception)
    {
        $errors = $exception->errors() ? array_first($exception->errors()) : [];

        return response()->json(
            [
                'message' => array_first($errors),
                'errors'  => $exception->errors(),
            ],
            $exception->status
        );
    }

    protected function convertExceptionToArray(Throwable $e)
    {
        $reflection = new ReflectionClass(get_class($e));

        return config('app.debug') ? [
            'message'   => $e->getMessage(),
            'exception' => get_class($e),
            'file'      => $e->getFile(),
            'line'      => $e->getLine(),
            'trace'     => collect($e->getTrace())->map(
                function ($trace) {
                    return Arr::except($trace, ['args']);
                }
            )->all(),
        ] : [
            'message' => $this->getMessage($e),
            'type'    => $reflection->getShortName(),
        ];
    }

    private function getErrorCode(Throwable $e)
    {
        if ($this->isHttpException($e)) {
            return $e->getStatusCode();
        }

        if ($e->getCode() && $e->getCode() >= 400 && $e->getCode() < 500) {
            return $e->getCode();
        }

        return 500;
    }

    private function getMessage(Throwable $e)
    {
        if ($e instanceof HttpResponseException) {
            return $e->getMessage();
        }

        if ($this->isHttpException($e) && $e->getStatusCode() && $e->getStatusCode() >= 400 && $e->getStatusCode() < 500) {
            return $e->getMessage();
        }

        if ($e->getCode() && $e->getCode() >= 400 && $e->getCode() < 500) {
            return $e->getMessage();
        }

        return 'Something error, please contact support@kemnaker.go.id.';
    }
}
