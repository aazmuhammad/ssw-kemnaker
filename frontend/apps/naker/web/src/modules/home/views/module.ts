import { NgModule } from '@angular/core';
import { HOME_PAGES } from '@naker/web/src/modules/home/views/pages';

@NgModule({
    declarations: [...HOME_PAGES],
})
export class NakerHomeViewModule {}
