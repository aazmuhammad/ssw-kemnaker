import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { HomePage } from '@naker/web/src/modules/home/views/pages/home.page';

const routes: Routes = [
    {
        path: '',
        component: HomePage,
    },
];

export const HOME_ROUTES: ModuleWithProviders = RouterModule.forChild(routes);
