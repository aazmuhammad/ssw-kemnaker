import { NgModule } from '@angular/core';
import { NakerHomeViewModule } from '@naker/web/src/modules/home/views/module';
import { HOME_ROUTES } from '@naker/web/src/modules/home/home.routes';

@NgModule({
    imports: [NakerHomeViewModule, HOME_ROUTES],
})
export class NakerHomeModule {}
