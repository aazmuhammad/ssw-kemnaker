import { LowonganPage } from '@naker/web/src/modules/lowongan/views/pages/lowongan.page';
import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes: Routes = [
    {
        path: '',
        component: LowonganPage,
    },
];

export const LOWONGAN_ROUTES: ModuleWithProviders = RouterModule.forChild(routes);
