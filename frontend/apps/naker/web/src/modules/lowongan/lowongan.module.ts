import { NgModule } from '@angular/core';
import { NakerLowonganViewModule } from '@naker/web/src/modules/lowongan/views/module';
import { LOWONGAN_ROUTES } from '@naker/web/src/modules/lowongan/lowongan.routes';

@NgModule({
    imports: [NakerLowonganViewModule, LOWONGAN_ROUTES],
})
export class NakerLowonganModule {}