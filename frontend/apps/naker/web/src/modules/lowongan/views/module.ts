import { NgModule } from '@angular/core';
import { LOWONGAN_PAGES } from '@naker/web/src/modules/lowongan/views/pages';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [...LOWONGAN_PAGES],
    imports: [
        CommonModule
    ],
})
export class NakerLowonganViewModule {}