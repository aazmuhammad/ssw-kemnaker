import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'naker-lowongan-page',
    templateUrl: './lowongan.page.html',
})

export class LowonganPage implements OnInit{

    pekerjaan = [];
    data = [
        { id : 1, name : 'Customer Service', tanggal : '22 Oktober 2020' },
        { id : 2, name : 'Staff Informasi dan Komunikasi', tanggal : '23 Oktober 2020' }
    ];
    
    ngOnInit(){
        this.pekerjaan = this.data;
    }

    cari(event : any){
        var filtersPekerjaan = [];
        var pekerjaan = this.data;
        pekerjaan.forEach((val:any,key:any) => {
            var thisIndex = filter(event.target.value,key,val);
            if(thisIndex != null){
                filtersPekerjaan.push(pekerjaan[thisIndex]);
            }
        });
        this.pekerjaan = filtersPekerjaan;
    }

    function filter(string,key,val){
       var name = val['name'].toLowerCase();
       string = string.toLowerCase();
       var include = name.includes(""+string+"");
       if(include){
           return key;
       }else{
           false;
       }
    }
}