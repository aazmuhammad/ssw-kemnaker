import { NgModule } from '@angular/core';
import { NakerAuthViewModule } from '@naker/web/src/modules/auth/views/module';
import { AUTH_ROUTES } from '@naker/web/src/modules/auth/auth.routes';

@NgModule({
    imports: [NakerAuthViewModule, AUTH_ROUTES],
})
export class NakerAuthModule {}
