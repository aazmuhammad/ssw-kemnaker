import { NgModule } from '@angular/core';
import { AUTH_PAGES } from '@naker/web/src/modules/auth/views/pages';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [RouterModule],
    declarations: [...AUTH_PAGES],
})
export class NakerAuthViewModule {}
