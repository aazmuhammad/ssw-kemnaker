import { RegisterPage } from '@naker/web/src/modules/auth/views/pages/register.page';

export const AUTH_PAGES = [RegisterPage];
