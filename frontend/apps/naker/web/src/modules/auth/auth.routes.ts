import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { RegisterPage } from '@naker/web/src/modules/auth/views/pages/register.page';

const routes: Routes = [
    {
        path: 'register',
        component: RegisterPage,
    },
];

export const AUTH_ROUTES: ModuleWithProviders = RouterModule.forChild(routes);
