import { NgModule } from '@angular/core';
import { NakerLoginViewModule } from '@naker/web/src/modules/login/views/module';
import { LOGIN_ROUTES } from '@naker/web/src/modules/login/login.routes';

@NgModule({
    imports: [NakerLoginViewModule, LOGIN_ROUTES],
})
export class NakerLoginModule {}
