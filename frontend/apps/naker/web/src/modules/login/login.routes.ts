import { LoginPage } from '@naker/web/src/modules/login/views/pages/login.page';
import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes: Routes = [
    {
        path: '',
        component: LoginPage,
    },
];

export const LOGIN_ROUTES: ModuleWithProviders = RouterModule.forChild(routes);
