import { NgModule } from '@angular/core';
import { LOGIN_PAGES } from '@naker/web/src/modules/login/views/pages';

@NgModule({
    declarations: [...LOGIN_PAGES],
})
export class NakerLoginViewModule {}