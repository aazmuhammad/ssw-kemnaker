import { NgModule } from '@angular/core';
import { NakerDashboardViewModule } from '@naker/web/src/modules/dashboard/views/module';
import { DASHBOARD_ROUTES } from '@naker/web/src/modules/dashboard/dashboard.routes';

@NgModule({
    imports: [NakerDashboardViewModule, DASHBOARD_ROUTES],
})
export class NakerDashboardModule {}
