import { NgModule } from '@angular/core';
import { DASHBOARD_PAGES } from '@naker/web/src/modules/dashboard/views/pages';

@NgModule({
    declarations: [...DASHBOARD_PAGES],
})
export class NakerDashboardViewModule {}
