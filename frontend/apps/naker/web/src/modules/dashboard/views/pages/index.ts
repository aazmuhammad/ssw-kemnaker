import { DashboardPage } from '@naker/web/src/modules/dashboard/views/pages/dashboard.page';

export const DASHBOARD_PAGES = [DashboardPage];
