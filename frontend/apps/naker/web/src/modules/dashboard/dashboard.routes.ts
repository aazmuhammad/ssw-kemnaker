import { DashboardPage } from '@naker/web/src/modules/dashboard/views/pages/dashboard.page';
import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes: Routes = [
    {
        path: '',
        component: DashboardPage,
    },
];

export const DASHBOARD_ROUTES: ModuleWithProviders = RouterModule.forChild(routes);
