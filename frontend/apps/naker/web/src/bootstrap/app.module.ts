import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { routing } from './app.routes';
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NakerAppTemplateModule } from '@naker/web/src/templates/app/app-template.module';
import { ServicePickerModule } from '@naker/service-picker';
import { HttpClientModule } from '@angular/common/http';
import { NakerLandingTemplateModule } from '@naker/web/src/templates/landing/landing-template.module';
import { NakerWebTemplateModule } from '@naker/web/src/templates/web/web-template.module';

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
        StoreDevtoolsModule.instrument(<any>{
            maxAgent: 25,
            logOnly: environment.production,
        }),
        StoreRouterConnectingModule,
        routing,
        HttpClientModule,
        NakerAppTemplateModule,
        NakerLandingTemplateModule,
        NakerWebTemplateModule,
        ServicePickerModule.forRoot(),
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
