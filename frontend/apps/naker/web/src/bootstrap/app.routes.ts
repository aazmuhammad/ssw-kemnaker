/*
 * This file is part of the Ubud package.
 *
 * (c) 2018 Ubud <https://github.com/ubud>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AppTemplate } from '@naker/web/src/templates/app/components/app.template';
import { LandingTemplate } from '@naker/web/src/templates/landing/components/landing.template';
import { WebTemplate } from '@naker/web/src/templates/web/components/web.template';

/**
 * @author  Iqbal Maulana <iq.bluejack@gmail.com>
 */
const routes: Routes = [
    {
        path: 'dashboard',
        component: AppTemplate,
        children: [
            {
                path: '',
                loadChildren: '@naker/web/src/modules/dashboard/dashboard.module#NakerDashboardModule',
            },
        ],
    },
    {
        path: 'auth',
        component: LandingTemplate,
        children: [
            {
                path: '',
                loadChildren: '@naker/web/src/modules/auth/auth.module#NakerAuthModule',
            },
        ],
    },
    {
        path: '',
        component: WebTemplate,
        children: [
            {
                path: '',
                loadChildren: '@naker/web/src/modules/home/home.module#NakerHomeModule',
            },
        ],
    },
    {
        path: 'login',
        component: LandingTemplate,
        children: [
            {
                path: '',
                loadChildren: '@naker/web/src/modules/login/login.module#NakerLoginModule',
            },
        ],
    },
    {
        path: 'lowongan',
        component: WebTemplate,
        children: [
            {
                path: '',
                loadChildren: '@naker/web/src/modules/lowongan/lowongan.module#NakerLowonganModule',
            },
        ],
    },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
