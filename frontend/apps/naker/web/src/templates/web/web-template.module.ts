import { NgModule } from '@angular/core';
import { WEB_TEMPLATE_COMPONENTS } from '@naker/web/src/templates/web/components';
import { RouterModule } from '@angular/router';
import { ServicePickerModule } from '@naker/service-picker';

@NgModule({
    imports: [RouterModule, ServicePickerModule],
    declarations: [...WEB_TEMPLATE_COMPONENTS],
    exports: [...WEB_TEMPLATE_COMPONENTS],
})
export class NakerWebTemplateModule {}
