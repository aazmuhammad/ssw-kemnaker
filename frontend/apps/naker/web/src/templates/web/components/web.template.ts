import { Component } from '@angular/core';

@Component({
    selector: 'naker-web-template',
    templateUrl: './web.template.html',
    styleUrls: ['./web.template.scss'],
})
export class WebTemplate {
    public year: number;

    public constructor() {
        this.year = new Date().getFullYear();
    }
}
