import { Component } from '@angular/core';

@Component({
    selector: 'naker-landing-template',
    templateUrl: './landing.template.html',
    styleUrls: ['./landing.template.scss'],
})
export class LandingTemplate {}
