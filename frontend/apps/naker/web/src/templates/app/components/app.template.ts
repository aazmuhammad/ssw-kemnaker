import { Component } from '@angular/core';

@Component({
    selector: 'naker-app-template',
    templateUrl: './app.template.html',
    styleUrls: ['./app.template.scss'],
})
export class AppTemplate {}
