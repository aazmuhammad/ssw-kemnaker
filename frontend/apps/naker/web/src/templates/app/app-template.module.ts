import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UbudTemplateModule } from '@ubud/template';
import { UbudTooltipModule } from '@ubud/tooltip';
import { UbudDropdownModule } from '@ubud/dropdown';
import { APP_TEMPLATE_COMPONENTS } from '@naker/web/src/templates/app/components';
import { ServicePickerModule } from '@naker/service-picker';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [CommonModule, RouterModule, UbudTemplateModule, UbudTooltipModule, UbudDropdownModule, ServicePickerModule],
    declarations: [...APP_TEMPLATE_COMPONENTS],
    exports: [...APP_TEMPLATE_COMPONENTS],
})
export class NakerAppTemplateModule {}
